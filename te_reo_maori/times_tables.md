Make a script that would print the numbers 1-12. 

`(tahi..(tekau ma rua)).tena_ano { |a| kupu a }`{{execute}}

What this is done is first create a range from 1(tahi) to 12(tekau ma rua). 

`tena_ano` will loop through each item in this range and execute it in the block of the `{}`.

The `a` in the pipes(`|`) is arbitrary, representing the current number in that loop.

`kupu` will print whatever you pass to it. If you want to make a string with multiple variables and text you can do this
with `"` and `#{}`. E.g,

Store a name in a variable called `ingoa`
`ingoa = 'Sam'`{{execute}}
Welcome that person.
`kupu "Haere mai #{ingoa}"`{{execute}}

For complex functions you can make loops within loops. For example:

Make a 12 times tables with

`
(tahi..(tekau ma rua)).tena_ano { |a| 
  (tahi..(tekau ma rua)).tena_ano { |b| 
    kupu "#{a} * #{b} = #{a * b}" 
  } 
}
`{{execute}}

