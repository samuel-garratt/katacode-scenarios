Quiz your knowledge of numbers by opening a quiz with

`pangarau`{{execute}}

You can execute this as many times as you want. You may get lost knowing which numbers are what. After a test,
find what the number is by typing it again and adding `.tau`.

For example `(rima tekau ma toru).tau`{{execute}}
