Install the te_reo_maori library with

`gem install te_reo_maori`{{execute}}

Start the interactive session by executing the `te_reo_maori` exe.

`te_reo_maori`{{execute}}

The help text displayed gives you some of the basic numbers and commands you can try in the terminal to the right.

You can type numbers and see how to pronounce them in Maori.
> Currently only 1 - 999 are supported

Try typing a single digit
`1`{{execute}} 
The first double digit `10`{{execute}}

Now a double digit, the number 11
`11`{{execute}}
See how `ma` is added to join the 2 numbers together.

Now with
`25`{{execute}}
`rua`(two) is in front of `takau`(10) and then we add a digit `rima`(5)

You can play with more numbers to see how they are formed.

Try some basic maths:

`34 + 55`{{execute}}

and multiplication

`12 * 11`{{execute}}

You can also type the numbers directly in Maori. 
`rua + toru`{{execute}}

The terminal assumes you don't have macrons on the keyboards but will echo them back to you in the result. 
E.g.,

`rima tekau ma tahi`{{execute}}

See the macron on `ma`.
